# MarkRestAssured

This is my example repository to showcase using rest-assured tests in a BDD(Behaviour Driven Development) approach using cucumber.

This is my first attempt at rest-assured and with creating BDD style tests

## Getting started

To run the tests locally you need to: 

    - Clone the repository from gitlab
    - Run "mvn clean install"
    - Run "mvn test" to run the tests on your machine

A test report should be produced in the "target/surefire-reports/TEST-CucumberRunnerTest.xml"
