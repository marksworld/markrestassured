import http from 'k6/http';

let body = {
    "username" : "",
    "password" : ""
}

const authUri = "https://test-api.k6.io/auth/token/login/";

export function getBearerToken() {
    let res = http.post(authUri , body)
    var token = res.json().access
    return token
}
