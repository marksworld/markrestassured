package com.example.markRestAssured;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarkRestAssuredApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarkRestAssuredApplication.class, args);
	}

}
