package com.example.markRestAssured.service;

import com.example.markRestAssured.api.model.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private List<User> userList;

    public UserService(){
        userList = new ArrayList<>();

        User user1 = new User(1, "Ida", 32,"ida@email.com");
        User user2 = new User(2, "Tom", 33,"tom@email.com");
        User user3 = new User(3, "Sarah", 44,"sarah@email.com");
        User user4 = new User(4, "James", 22,"james@email.com");
        User user5 = new User(5, "Augustine", 18,"augustine@email.com");

        userList.addAll(Arrays.asList(user1,user2,user3,user4,user5));

    }

    public Optional<User> getUser(int id) {
        Optional optional = Optional.empty();
        for (User user: userList) {
            if(id == user.getId()){
                optional = Optional.of(user);
                return optional;
            }
        }
        return optional;
    }
}
