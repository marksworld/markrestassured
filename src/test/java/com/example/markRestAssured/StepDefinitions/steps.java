package com.example.markRestAssured.StepDefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.minidev.json.JSONObject;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Assertions;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class steps {

    private static String token;
    private static Response resp;
    private static JsonPath jsonResp;
    private static String uri;

    @Given("I am an authorized user")
    public void i_am_an_authorized_user() {

        RestAssured.baseURI = "https://test-api.k6.io";

        JSONObject requestParams = new JSONObject();
        requestParams.put("username", "TestUser");
        requestParams.put("password", "SuperCroc2020");

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(requestParams)
                .when()
                .post(baseURI+ "/auth/token/login/")
                .then().extract().response();

        JsonPath jsonResponse = response.jsonPath();
        token = jsonResponse.get("access").toString();
    }

    @Given("a valid token is used")
    public void a_valid_token_is_used() {
        //add checks for validation of token
        System.out.println("token is valid");
    }
    @Given("i am an unauthorized user with {string} in my username request")
    public void i_am_an_unauthorized_user_with_in_my_username_request(String username) {
        RestAssured.baseURI = "https://test-api.k6.io";

        JSONObject requestParams = new JSONObject();
        requestParams.put("username", username);
        requestParams.put("password", "SuperCroc2020");

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(requestParams)
                .when()
                .post(baseURI+ "/auth/token/login/")
                .then().extract().response();

        resp = response;

        jsonResp = resp.jsonPath();
    }

    @Given("i am an unauthorized user with {string} in my password request")
    public void i_am_an_unauthorized_user_with_in_my_password_request(String password) {
        RestAssured.baseURI = "https://test-api.k6.io";

        JSONObject requestParams = new JSONObject();
        requestParams.put("username", "TestUser");
        requestParams.put("password", password);

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(requestParams)
                .when()
                .post(baseURI+ "/auth/token/login/")
                .then().extract().response();

        resp = response;
        jsonResp = resp.jsonPath();
    }

    @Given("i am an unauthorized user with integer {int} as my username")
    public void i_am_an_unauthorized_user_with_integer_as_my_username(Integer username) {
        RestAssured.baseURI = "https://test-api.k6.io";

        JSONObject requestParams = new JSONObject();
        requestParams.put("username", username);
        requestParams.put("password", "SuperCroc2020");

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(requestParams)
                .when()
                .post(baseURI+ "/auth/token/login/")
                .then().extract().response();

        resp = response;
        jsonResp = resp.jsonPath();
    }

    @Given("I am an unauthorized user with an empty Json request")
    public void i_am_an_unauthorized_user_with_an_empty_json_request() {
        RestAssured.baseURI = "https://test-api.k6.io";

        JSONObject requestParams = new JSONObject();

        Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(requestParams)
                .when()
                .post(baseURI+ "/auth/token/login/")
                .then().extract().response();

        resp = response;
        jsonResp = resp.jsonPath();
    }

    @Given("I have an unauthorized token")
    public void I_have_an_unauthorized_token(){
        token = "INVALID_TOKEN";
    }
    @Given("an id of {int} is added to crocs request")
    public void an_id_of_is_added_to_crocs_request(Integer id) {
        uri = "/public/crocodiles/" + id;
    }
    @Given("a DELETE request is made to crocs endpoint")
    public void a_delete_request_is_made_to_crocs_endpoint() {
        RestAssured.baseURI = "https://test-api.k6.io";
        Response response = given()
                .headers(
                        "Authorization",
                        "Bearer " + token,
                        "Content-Type",
                        ContentType.JSON,
                        "Accept",
                        ContentType.JSON)
                .and()
                .when()
                .delete(baseURI + "/my/crocodiles/")
                .then()
                .extract().response();

        resp = response;
        jsonResp = resp.jsonPath();
    }

    @Given("a croc is added with id {int}")
    public void a_croc_is_added_with_id(Integer id) {
        RestAssured.baseURI = "https://test-api.k6.io";

        JSONObject requestParams = new JSONObject();
        requestParams.put("id", "TestUser");
        requestParams.put("name", "Sobek");
        requestParams.put("sex", "F");
        requestParams.put("date_of_birth", "1854-09-02");
        requestParams.put("age", 169);

        // abstract this test object??

        Response response = given()
                .headers(
                        "Authorization",
                        "Bearer " + token,
                        "Content-Type",
                        ContentType.JSON,
                        "Accept",
                        ContentType.JSON)
                .and()
                .body(requestParams)
                .when()
                .post(baseURI + "/public/crocodiles/" + id)
                .then()
                .extract().response();

        Response responseCroc = given()
                .headers(
                        "Authorization",
                        "Bearer " + token,
                        "Content-Type",
                        ContentType.JSON,
                        "Accept",
                        ContentType.JSON)
                .and()
                .when()
                .get(baseURI + "/public/crocodiles/" + id)
                .then()
                .extract().response();

        resp = responseCroc;
        jsonResp = resp.jsonPath();

    }
    @When("a valid GET request is made")
    public void a_valid_get_request_is_made() {
        RestAssured.baseURI = "https://test-api.k6.io";
        Response response = given()
                .headers(
                        "Authorization",
                        "Bearer " + token,
                        "Content-Type",
                        ContentType.JSON,
                        "Accept",
                        ContentType.JSON)
                .and()
                .when()
                .get(baseURI + uri)
                .then()
                .extract().response();

        resp = response;
        jsonResp = resp.jsonPath();
    }
    @When("a valid request is made to crocodiles endpoint")
    public void a_valid_request_is_made_to_crocodiles_endpoint() {

        RestAssured.baseURI = "https://test-api.k6.io";

        Response response = given()
                .headers(
                        "Authorization",
                        "Bearer " + token,
                        "Content-Type",
                        ContentType.JSON,
                        "Accept",
                        ContentType.JSON)
                .and()
                .when()
                .get(baseURI + "/my/crocodiles/")
                .then()
                .extract().response();

        resp = response;
        jsonResp = resp.jsonPath();

    }
    @Then("a {int} response is returned")
    public void a_response_is_returned(Integer statusCode) {
        Assertions.assertEquals(statusCode, resp.statusCode());
    }

    @Then("correct crocs data is returned")
    public void a_correct_data_is_returned() {
        Assertions.assertEquals("Babcy Croc", jsonResp.get("name[1]"));
    }
    @Then("correct {string} schema is matched in response")
    public void correct_schema_is_matched_in_response(String schema) {
        MatcherAssert.assertThat(
                "Validate json schema",
                resp.getBody().asString(),
                JsonSchemaValidator.matchesJsonSchemaInClasspath(schema+".json")
        );
    }

    @Then("a no active account message is returned")
    public void a_no_active_account_message_is_returned() {
        Assertions.assertEquals("No active account found with the given credentials", jsonResp.get("detail"));
    }

    @Then("missing fields message is returned")
    public void missing_fields_message_is_returned() {
        Assertions.assertEquals("This field is required.", jsonResp.get("username[0]"));
        Assertions.assertEquals("This field is required.", jsonResp.get("password[0]"));
    }

    @Then("an invalid token message is returned")
    public void an_invalid_token_message_is_returned(){
        Assertions.assertEquals("Given token not valid for any token type", jsonResp.get("detail"));
        Assertions.assertEquals("token_not_valid", jsonResp.get("code"));
        Assertions.assertEquals("Token is invalid or expired", jsonResp.get("messages[0].message"));
    }
    @Then("correct croc data is returned")
    public void correct_croc_data_is_returned() {
        int response = jsonResp.get("id");
        Assertions.assertEquals(1, response);
        // add more assertions here
    }
    @Then("a croc with id of {int} is returned")
    public void a_croc_with_id_of_is_returned(Integer id) {
        int responseId = jsonResp.get("id");
        Assertions.assertEquals(7, responseId);
        int responseAge = jsonResp.get("age");
        Assertions.assertEquals(169, responseAge);
        Assertions.assertEquals("Sobek", jsonResp.get("name"));
        Assertions.assertEquals("F", jsonResp.get("sex"));
        Assertions.assertEquals("1854-09-02", jsonResp.get("date_of_birth"));
    }


}
