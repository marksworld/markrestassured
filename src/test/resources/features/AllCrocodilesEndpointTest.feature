Feature: All crocodiles endpoint tests

    Background: User generates a token
        Given I am an authorized user
    Scenario: A valid GET request will return list of crocodiles
        Given a valid token is used
        When a valid request is made to crocodiles endpoint
        Then a 200 response is returned
        And correct crocs data is returned
        And correct "valid_crocs_schema" schema is matched in response

    Scenario: An invalid token in request returns 401 unauthorized
        Given I have an unauthorized token
        When a valid request is made to crocodiles endpoint
        Then a 400 response is returned
        And an invalid token message is returned
        And correct "invalid_token" schema is matched in response

    Scenario: Invalid request method returns correct error response
        Given a DELETE request is made to crocs endpoint
        Then a 405 response is returned





