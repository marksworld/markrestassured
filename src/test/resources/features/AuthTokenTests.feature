Feature: Generating auth token tests
  Scenario: An invalid Username is provided for a token request
    Given i am an unauthorized user with "INVALID_USERNAME" in my username request
    Then a 401 response is returned
    And a no active account message is returned

  Scenario: An invalid Password is provided for a token request
    Given i am an unauthorized user with "INVALID_PASSWORD" in my password request
    Then a 401 response is returned
    And a no active account message is returned

  Scenario: An invalid request type is added to request
    Given i am an unauthorized user with integer 12345 as my username
    Then a 401 response is returned
    And a no active account message is returned

  Scenario: Missing username and password fields in token request
    Given I am an unauthorized user with an empty Json request
    Then a 400 response is returned
    And missing fields message is returned
