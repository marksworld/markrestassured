Feature: Individual Crocodile endpoint tests

  Background: User generates a token
    Given I am an authorized user
  Scenario: Valid id in parameter returns correct crocodile id
    Given an id of 1 is added to crocs request
    When a valid GET request is made
    Then a 200 response is returned
    And correct croc data is returned
    And correct "valid_croc_schema" schema is matched in response

  Scenario: No matching Id returns a 404 response
    Given an id of 9999 is added to crocs request
    When a valid GET request is made
    Then a 404 response is returned

  Scenario: A croc can be added to the list of crocs
    Given a croc is added with id 7
    When an id of 7 is added to crocs request
    Then a croc with id of 7 is returned





